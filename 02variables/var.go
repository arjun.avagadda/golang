package main

import "fmt"

const Heloos string = "constant var outside method"

func main() {
	// string type
	var usrname string = "arjun"
	fmt.Println(usrname)

	// int data type
	var id int = 17
	fmt.Println(id)

	// int data type
	var idfloat float32 = 17.45
	fmt.Println(idfloat)

	name := "arjun"
	fmt.Printf("type of var : %T \n", name)

	var mykey = "some text here"

	fmt.Println(mykey)

	mykey = "texttxtx"

	fmt.Println(mykey)

	fmt.Println(Heloos)

}
