package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {

	readobj := bufio.NewReader(os.Stdin)

	input, err := readobj.ReadString('\n')

	fmt.Println(input, err)

}
